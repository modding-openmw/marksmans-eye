# Marksman's Eye

Find a powerful artifact that magically enhances the player's vision with a zoom effect while aiming a bow or crossbow. Requires OpenMW-Lua.

Based on [Advanced Camera / Bow Aiming](https://gitlab.com/ptmikheev/openmw-lua-examples#advanced-camera) by ptmikheev.

#### Credits

##### Original concept, code

ptmikheev

##### Mesh/Texture credits

[Practice Dummies & Targets](https://stuporstar.sarahdimento.com/other-mods/practice-dummies-targets/) mod by **Stuporstar**

**Original Practice Dummies & Targets mod credits and thanks (thank you, again!):**

Thanks to Reizeron for improving the meshes for 2.0.

Thanks to abot for writing the Wrye Mash replacer scripts.

Thanks to Abiel0530 for suggesting the 2.0 update.

Thanks, Spirithawke, for fixing the practice target meshes for version 2.0.

Kiteflyer made the original practice target. I did the animations.

AcidBasick made the original mod Acidbasick's Animated Practice Dummies 1.0.

Bud_Lyte_kNight's scripted version inspired me to make this one.

Fliggerty for his excellent suggestion on how to check for a successful hand-to-hand hit.

HeyYou and NarfBlat for helping me work out a way of modding heath without killing the player (older versions).

##### Marksman's Eye author

johnnyhostile

##### Localizations

PT_BR: Karolz

DE: Atahualpa

ES: Drumvee

EN: johnnyhostile

FR, NL: Hitobaka

PL: stahs, emcek

SV: Lysol

ZH, ZH-CN: noctune

##### Special Thanks

ptmikheev (for creating OpenMW-Lua and answering my questions)

eddie5 (wisdom)

Ignatious (wisdom, dialogue troubleshooting help)

EvilEye ([CS.js](https://assumeru.gitlab.io/cs.js/))

Greatness7 ([tes3conv](https://github.com/Greatness7/tes3conv))

Benjamin Winger ([DeltaPlugin](https://gitlab.com/bmwinger/delta-plugin/))

#### Installation

**OpenMW 0.48 or newer is required!**

1. Download the mod from [this URL](https://modding-openmw.gitlab.io/marksmans-eye/)
1. Extract the zip to a location of your choosing, examples below:

        # Windows
        C:\games\OpenMWMods\Camera\MarksmansEye

        # Linux
        /home/username/games/OpenMWMods/Camera/MarksmansEye

        # macOS
        /Users/username/games/OpenMWMods/Camera/MarksmansEye

1. Add the appropriate data path to your `opemw.cfg` file (e.g. `data="C:\games\OpenMWMods\Camera\MarksmansEye"`)
1. Add `content=marksmans-eye.omwaddon` and `content=marksmans-eye.omwscripts` to your load order in `openmw.cfg` or enable them via OpenMW-Launcher
1. Enjoy!

#### Report A Problem

If you've found an issue with this mod, or if you simply have a question, please use one of the following ways to reach out:

* [Open an issue on GitLab](https://gitlab.com/modding-openmw/marksmans-eye/-/issues)
* Email `marksmans-eye@modding-openmw.com`
* Contact the author on Discord: `johnnyhostile#6749`
* Contact the author on Libera.chat IRC: `johnnyhostile`

#### Development

This mod is written with [Fennel](https://fennel-lang.org/). Since OpenMW cannot load Fennel source files directly, it is compiled to Lua before being zipped for use. **Do not send patches for the compiled Lua source!**

Please format all code with [`fnlfmt`](https://git.sr.ht/~technomancy/fnlfmt#usage).

See [`build.sh`](build.sh) for details on how the shipped Lua is generated.

#### Planned Features

* A shader that slightly blurs the edge of the screen while zooming
* A better icon and/or model + texture for the Marksman's Eye item itself
* [Request a feature!](https://gitlab.com/modding-openmw/marksmans-eye/-/issues/new)
