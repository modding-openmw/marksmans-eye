#!/usr/bin/env bash
set -eu -o pipefail

mod_name=marksmans-eye
file_name=${mod_name}.zip

fnl_src=${mod_name}.fnl
lua_out=scripts/${mod_name}/player.lua

cat > version.txt <<EOF
Mod version: $(git describe --tags || echo DEVELOPMENT)
EOF

mkdir -p scripts/${mod_name}
cat > ${lua_out} <<EOF
--[[
!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!! NOTE !!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!

This Lua was generated from Fennel.

Fennel resources:
https://fennel-lang.org/setup
https://fennel-lang.org/tutorial
https://fennel-lang.org/lua-primer
https://fennel-lang.org/reference

Original Fennel source:

$(cat ${fnl_src})
]]--
EOF

fennel --compile ${fnl_src} >> ${lua_out}

mv CHANGELOG.md LICENSE README.md docs/

zip --must-match --recurse-paths ${file_name} docs icons l10n meshes textures ${lua_out} ${mod_name}.omwaddon ${mod_name}.omwscripts version.txt
sha256sum ${file_name} > ${file_name}.sha256sum.txt
sha512sum ${file_name} > ${file_name}.sha512sum.txt

cd docs/
mv CHANGELOG.md LICENSE README.md ..
