## Marksman's Eye Walkthrough

<span class="bold center" style="font-size: 5.0em;" title="It should go without saying for a walkthrough, but hey">Caution!!!! Spoilers ahead!</span>

#### A Little Secret...

There's a chance that someone who is of a class that might be interested in marksman will mention something about a little secret. New players might ask around at Arrille's Tradehouse in Seyda Neen.

#### Where To Go

Your informer tells you that Gilyan Sedas of Balmora knows about something a marksman would be interested in. He's in the Morag Tong Guildhall.

#### Spill The Beans

Gilyan Sedas is a bit of a tough nut to crack, but he lets slip the location of a powerful artifact if he likes you enough (a disposition of 75 or higher is required).

#### Acquire The Eye

The "Marksman's Eye" artifact is evidently in Sarys Ancestral Tomb, west of Seyda Neen. The tomb is filled with creatures, but you should be able to find a curious item that looks like an archery target.

#### Using The Power

The "Marksman's Eye" artifact imparts enhanced vision to the player, allowing for a "zoom" effect when aiming with a bow or crossbow. The player _must_ be holding the Marksman's Eye item.

The magnitude of this effect scales with the player's speed attribute and marksman ability; the marksman skill weighs more in this calculation.
