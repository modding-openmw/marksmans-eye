#!/usr/bin/env bash
set -euo pipefail
#
# This script builds the project website. It downloads soupault as needed and
# then runs it, the built website can be found at: web/build
#

this_dir=$(realpath "$(dirname "${0}")")
cd "${this_dir}"

soupault_version=4.11.0
soupault_pkg=soupault-${soupault_version}-linux-x86_64.tar.gz
soupault_path=./soupault-${soupault_version}-linux-x86_64

shas=https://github.com/PataphysicalSociety/soupault/releases/download/${soupault_version}/sha256sums
spdl=https://github.com/PataphysicalSociety/soupault/releases/download/${soupault_version}/${soupault_pkg}

if ! [ -f ${soupault_path}/soupault ]; then
    curl -sLO ${shas}
    curl -sLO ${spdl}
    tar xf ${soupault_pkg}
    grep 'linux-x86_64' sha256sums | sha256sum -c -
fi

echo "Releases without a download link can be downloaded as a dev build from the link above." > site/changelog.md
grep -v "## Marksman's Eye" ../CHANGELOG.md >> site/changelog.md
cat > site/index.md <<EOF
<div class="center">
  <img src="/img/marksmans-eye.gif" title="The mod in action" />
</div>
<div id="modathon" class="center">
  <img src="/img/cheev2022-banner.png" title="Part of the May Modathon 2022!" />
  <img src="/img/cheev2022-cluttermonkey.png" title="Part of the May Modathon 2022!" />
  <img src="/img/cheev2022-noviceofmadness.png" title="Part of the May Modathon 2022!" />
  <img src="/img/cheev2022-adventuresawait.png" title="Part of the May Modathon 2022!" />
  <img src="/img/cheev2022-letmetweakthat.png" title="Part of the May Modathon 2022!" />
  <img src="/img/modathon-2022.png" title="Part of the May Modathon 2022!" />
</div>
EOF
grep -v "# Marksman's Eye" ../README.md >> site/index.md
cp ../CHANGELOG.md site/changelog.md
cp ../docs/EVENTS_AND_INTERFACE.md site/events_and_interface.md
cp ../README.md site/readme.md
cp ../docs/WALKTHROUGH.md site/walkthrough.md

PATH=${soupault_path}:$PATH soupault "$@"
